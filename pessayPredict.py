#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pickle

import numpy as np
import sys
from sklearn import ensemble
from sklearn.externals import joblib
import pandas as pd

from utilities import normalization

def get_DataFrame(dataset_names,names_columns):

    head_columns = np.array(
        [line.strip() for line in open(names_columns, 'r')])
    df = pd.read_csv(dataset_names, names=head_columns)
    return df


def deleteRowsEssay(df):

    df_indexes = df[(df['nota norma culta'] >= 1.5) & ( df['erros ortograficos'] + df['erros gramaticais cogroo']  >= 7)]
    #print('Redações com nota competência 1 maior ou igual a 1.5 com mais de 7 erros (ortográficos e gramaticais): {0} '.format(df_indexes.shape))
    df = df.drop(df_indexes.index)

    df_indexes = df[(df['nota norma culta'] <= 1.0) & ( df['erros gramaticais cogroo'] + df['erros ortograficos'] <= 3)]
    #print('Redações com nota competência 1 igual a 1.0 com 0 erros gramaticais: {0} '.format(df_indexes.shape))
    df = df.drop(df_indexes.index)

    return df


def saveModel(clf,filename):
    _ = joblib.dump(clf, filename, compress=9)

def loadModel(filename):
    return joblib.load(filename)

def save(min_max_scaler):
    with open('model/demo.pkl','wb') as output:
        pickle.dump(min_max_scaler, output,pickle.HIGHEST_PROTOCOL)

def load(filename):
    with open(filename, 'rb') as input:
        min_max_scaler =  pickle.load(input)
    return min_max_scaler

def treino_model():

    params = {'n_estimators': 1200, 'max_depth': 3, 'subsample': 0.5,
              'learning_rate': 0.01, 'min_samples_leaf': 1, 'random_state': 3}
    clf = ensemble.GradientBoostingClassifier(**params)

    dataset_escola = './files/dataset_treino/dataset_CoGrOO_escola.csv'
    features = './files/dataset_treino/features_cogroo.txt'

    df = get_DataFrame(dataset_escola,features)
    df = deleteRowsEssay(df)

    features_names = np.array(df.columns)[1:-4]

    y = np.array(df['nota norma culta']) * 10
    z = df['redacao']
    df.drop(['redacao', 'nota norma culta', 'nota total', 'url', 'diretorio'], inplace=True, axis=1)
    data = np.array(df.values, dtype='float')
    X, min_max_scaler = normalization(data)
    print('Treinando modelo...')
    clf.fit(X, y)

    save_model(clf, min_max_scaler)

    return clf, min_max_scaler


def save_model(clf, min_max_scaler):
    save(min_max_scaler)
    print(type(min_max_scaler))
    filename = 'model/classifier.joblib.pkl'
    saveModel(clf, filename)


def teste(raiz):
    clf,min_max_scaller = load_model()
    df = get_DataFrame(raiz + '\\dataset.csv','./files/dataset_treino/features_cogroo.txt')
    z = df['redacao']
    df.drop(['redacao', 'nota norma culta', 'nota total', 'url', 'diretorio'], inplace=True, axis=1)
    data = np.array(df.values, dtype='float')
    X, _ = normalization(data, min_max_scaller)
    y_pred = clf.predict(X)

    file = open(raiz + 'notas.csv','w')

    for idx,nota in enumerate(y_pred):
        file.write('{0};{1}\n'.format(z[idx][:-4],(nota*10/2)))
    file.close()
    print('Arquivos notas.csv gerado...')
    return y_pred[0]


def load_model():
    filename = './model/classifier.joblib.pkl'
    filename1 = './model/demo.pkl'
    clf = loadModel(filename)
    min_max_scaler = load(filename1)
    return clf, min_max_scaler