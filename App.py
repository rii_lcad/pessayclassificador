from flask import Flask, request, jsonify
import pessayPredict as pe
import os, json, tempfile
from flask_cors import CORS

app = Flask("pRedacao")
CORS(app)

@app.route('/home')
def home():
    return "ola mundo"

@app.route('/obterNota', methods=['POST'])
def obterNotas():
    with tempfile.TemporaryDirectory() as tmpdirname:
        with open(tmpdirname + "\\dataset.csv",'w') as arquivo:
            arquivo.write(request.form['features'])
        notaComp1 = pe.teste(tmpdirname)
        print(arquivo)
    return jsonify({"nota" : notaComp1})

@app.route('/treinar', methods=['GET'])
def treinat_modelo():
    pe.treino_model()
    return "<b>Modelo Treinado com exito!</b>"

if __name__=='__main__':    
    app.run('0.0.0.0',debug=True, port=8081)
    pe.treino_model()