import matplotlib
from sklearn import preprocessing

from demo_dataframe import get_DataFrame, insertEssay, deleteRowsEssay
from similarity import Similarity
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick



def normalization(data_set, scaler=None):
    if scaler is None:
        min_max_scaler = preprocessing.MinMaxScaler()
        min_max_scaler.fit(data_set)
    else:
        min_max_scaler = scaler
    X_train_min_max = min_max_scaler.transform(data_set)
    return X_train_min_max, min_max_scaler

def essay_zero(df):
    df_zero = df[ (df['nota norma culta'] == 0) & (df['nota total'] == 0) ]
    print('Redações zeradas: {0}'.format(df_zero.shape[0]))
    print(df_zero['redacao'])


def plotar_grafico_erros(df,feature):

    df_copy = df.copy()
    colors = ['r','y','c','g','b']
    notas = [0.0,0.5,1.0,1.5,2.0]
    x_maior = 0
    y_maior = 0
    for i,nota in enumerate(notas):
        df = df_copy [ df_copy['nota norma culta'] == nota ]
        df = df.filter(items=[feature, 'redacao'])
        #g = df.groupby([feature, 'redacao']).count()
        g = df.groupby(feature)
        g1 = df.groupby(feature).count()

        x = np.array(g1.index)
        y = np.array(g1[g1.columns[0]])
        y = y / y.sum()

        x_maior = x.max() if x.max() > x_maior else x_maior
        y_maior = y.max() if y.max() > y_maior else y_maior
        print('Nota: {0}: Quantidade: {1}'.format(nota,df.shape[0]) )
        print('Média de {0} : {1} '.format(feature,x.mean()))
        print('Desvio padrão de {0} : {1} '.format(feature, x.std()))
        #print(g)
        for key, item in g:
            print(g.get_group(key))
        print(g1)
        print("-"*80)

        plt.bar(x, y, label=notas[i], color=colors[i])

    plt.yticks(np.arange(0, 1.0, 0.05))
    plt.xticks(np.arange(0, x_maior + 2, 1))
    plt.xlabel('quantidade de ' + feature)
    plt.ylabel('quantidade de redações')
    plt.legend(loc='best')
    plt.grid(True)
    plt.show()


def to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = str(100 * y)

    # The percent symbol needs escaping in latex
    if matplotlib.rcParams['text.usetex'] is True:
        return s + r'$\%$'
    else:
        return s + '%'

def density_for_classes(x, y, level, print_steps=False):
    x = np.insert(x, x.shape[1], y, axis=1)
    x_classes = x[x[:, -1] == level]
    centroid = np.mean(x_classes[:, 0:-1], axis=0)  # centroid writer
    if print_steps:
        print('Centroid grade {0}'.format(level))
        print(centroid)
        # pw.write('Centroid grade {0}\n'.format(grade))
        # pw.write('[')
        # for c in centroid:
        #     pw.write('{0}, '.format(c))
        # pw.write(']\n')
    summation = 0.0
    similarity = Similarity()
    for doc in x_classes:
        summation += similarity.cosine_similarity(doc[0:-1], centroid)
    density = summation / len(x_classes)
    if print_steps:
        # pw.write('Density : {0}\n'.format(density))
        print(density)
    return density

if __name__ == '__main__':

    features = ('/home/celso/Documents/datasets/features_regra.txt', '/home/celso/Documents/datasets/features_cogroo.txt')

    datasets_escola = ('/home/celso/Documents/datasets/escola/regra/dataset_ReGra_escola.csv', '/home/celso/Documents/datasets/escola/cogroo/dataset_CoGrOO_escola.csv')

    datasets_uol = ('/home/celso/Documents/datasets/uol/regra/dataset_ReGra_uol.csv', '/home/celso/Documents/datasets/uol/cogroo/dataset_CoGrOO_uol.csv')

    datasets_notas_mil = ('/home/celso/Documents/experimentos/redacoes_nota_mil/ReGra_201752_5146/dataset_ReGra_nota_mil.csv',
                          '/home/celso/Documents/experimentos/redacoes_nota_mil/CoGrOO_201752_43723/dataset_CoGrOO_nota_mil.csv')

    df = get_DataFrame(datasets_uol[1], features[1])

    # df1 = df[df['nota total'] == 0.0]
    # print('Redações com nota total zero: {0}'.format(df1.shape))
    #
    # df = df[ df['nota total'] > 0.0 ]
    # print('Após retirada das redações zeradas: {0}'.format(df.shape))
    #
    # df = insertEssay(df, datasets_notas_mil[1], features[1])
    #df = deleteRowsEssay(df)

    df['erros'] = df['erros ortograficos'] + df['erros gramaticais cogroo']

    for feature in ['erros ortograficos','erros gramaticais cogroo','erros']:
        plotar_grafico_erros(df,feature )
