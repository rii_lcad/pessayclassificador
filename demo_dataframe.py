import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats, integrate
import seaborn as sns


def data_distribution(df):
    pass
    dados = df.iloc[:,[1,3]]
    # sns.kdeplot(df['erro gramatical'])
    # sns.lmplot('erro gramatical','nota norma culta', data=df, fit_reg=True)
    # # histograma
    plt.hist(df['erros gramaticais'], alpha= .3)
    sns.rugplot(df['erro gramaticais'])
    plt.show()

def removeFeatures(df):

    features_names = np.array(df.columns)[1:-4]
    n = 0
    list = []
    print(df.shape)
    for name in features_names:
        if np.sum(df[name]) == 0:
            n += 1
            list.append(name)
    df.drop(list, inplace=True, axis=1)

    print('{0} features excluídas'.format(n))
    print(*list, sep=', ')
    print(df.shape)

    return df



def get_DataFrame(dataset_names,names_columns):

    head_columns = np.array(
        [line.strip() for line in open(names_columns, 'r')])
    df = pd.read_csv(dataset_names, names=head_columns)
    print('Dataset name: {0}'.format(df.shape))
    return df


def deleteRowsEssay(df):

    df_indexes1 = df[(df['nota norma culta'] >= 1.5) & ( df['erros ortograficos'] + df['erros gramaticais cogroo']  >= 4)]
    print('Redações com nota competência 1 maior ou igual a 1.5 com mais de 7 erros (ortográficos e gramaticais): {0} '.format(df_indexes1.shape))
    df = df.drop(df_indexes1.index)


    df_indexes2 = df[(df['nota norma culta'] <= 1.0) & ( df['erros gramaticais cogroo'] + df['erros ortograficos'] <= 2)]
    print('Redações com nota competência 1 menor ou igual a 1.0 com menos de 3 erros (gramaticais e ortográficos): {0} '.format(df_indexes2.shape))
    df = df.drop(df_indexes2.index)

    # df1 = df_indexes1.append(df_indexes2)
    # df1 = df1[['redacao', 'nota norma culta']]
    # print(df1.shape)
    # for nota,redacao in zip(df1['redacao'], df1['nota norma culta']):
    #     print('{0} : {1}\n'.format(redacao, nota))

    print(df.shape)

    return df


def insertEssay(df, dataset_new,names_columns):
    head_columns = np.array(
        [line.strip() for line in open(names_columns, 'r')])
    df_new = pd.read_csv(dataset_new, names=head_columns)
    #df_new = df_new[ df_new['nota norma culta'] < 1.0 ]
    df_new = df_new[( df_new['erros ortograficos'] + df_new['erros gramaticais regra']  <= 1)]
    print('Redações notas incluídas: {0}'.format(df_new.shape))
    df = df.append(df_new)
    print(df.shape)
    return df



