FROM python:3.7.1
WORKDIR /app
ADD . /app
#pip install --upgrade pip
RUN pip install -r requirements.txt
ENV NAME World
EXPOSE 8081
CMD ["python", "App.py"]